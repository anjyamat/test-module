(function(){
    console.log('test123 '+process.argv[3])
    //Daclaring variables
    var fs = require('fs');
    var reptxt = require('../bin/replaceme.js');
    var filedata;
    
    //Requiring files
    
    
    //Reading file test.txt
    fs.readFile(process.argv[2], 'utf8', function(err, data){
        if(err){
            console.error("Could not open file: %s", err);
            process.exit(1);
        }
        // calling replacement function
        console.log(reptxt.replaceme(data,process.argv[3],process.argv[4]));
        filedata = reptxt.replaceme(data,process.argv[3],process.argv[4]);
    
    
        fs.writeFile('out.txt', filedata, function(err){
           if(err){
               console.error("Error saving file %s". err);
               process.exit(1);
           } else{
               console.log('out.txt file saved');
           }
        });
        
    });    
}).call(this)